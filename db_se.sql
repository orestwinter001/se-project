-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 11, 2019 at 05:17 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_se`
--

-- --------------------------------------------------------

--
-- Table structure for table `dental_procedure`
--

CREATE TABLE `dental_procedure` (
  `procedure_id` int(11) NOT NULL,
  `procedure_name` varchar(191) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dental_procedure`
--

INSERT INTO `dental_procedure` (`procedure_id`, `procedure_name`) VALUES
(1, 'Extraction');

-- --------------------------------------------------------

--
-- Table structure for table `dental_record`
--

CREATE TABLE `dental_record` (
  `record_id` int(20) NOT NULL,
  `patient_id` int(20) NOT NULL,
  `procedure_id` int(20) NOT NULL,
  `dentist_id` int(20) NOT NULL,
  `complain` varchar(191) NOT NULL,
  `amount` varchar(100) NOT NULL,
  `date` varchar(20) NOT NULL,
  `time` varchar(20) NOT NULL,
  `encoded_by` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dental_record`
--

INSERT INTO `dental_record` (`record_id`, `patient_id`, `procedure_id`, `dentist_id`, `complain`, `amount`, `date`, `time`, `encoded_by`) VALUES
(1, 1, 1, 1, 'Tootache', '1000', '9/11', '10:46', 1);

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

CREATE TABLE `invoice` (
  `invoice_id` int(11) NOT NULL,
  `patient_id` int(11) NOT NULL,
  `total_amount` double NOT NULL,
  `date` varchar(50) NOT NULL,
  `time` varchar(20) NOT NULL,
  `receipt_number` varchar(191) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `patient_profile`
--

CREATE TABLE `patient_profile` (
  `patient_id` int(11) NOT NULL,
  `patient_name` varchar(191) NOT NULL,
  `patient_address` varchar(191) NOT NULL,
  `patient_contact` varchar(20) NOT NULL,
  `patient_age` int(10) NOT NULL,
  `patient_gender` varchar(20) NOT NULL,
  `patient_occupation` varchar(191) NOT NULL,
  `status` varchar(20) NOT NULL,
  `date` varchar(50) NOT NULL,
  `time` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `patient_profile`
--

INSERT INTO `patient_profile` (`patient_id`, `patient_name`, `patient_address`, `patient_contact`, `patient_age`, `patient_gender`, `patient_occupation`, `status`, `date`, `time`) VALUES
(1, 'Juan Dela Cruz', 'Davao', '090909', 47, 'M', 'Business Owner', 'Married', '9/11/2019', '7:50 AM'),
(2, 'Juan Cena', 'toril', '09909', 39, 'M', 'Engineer', 'Married', '9/11/2019', '9:05');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(191) NOT NULL,
  `address` varchar(250) NOT NULL,
  `contact` varchar(20) NOT NULL,
  `username` varchar(191) NOT NULL,
  `role` int(11) NOT NULL,
  `password` varchar(191) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `address`, `contact`, `username`, `role`, `password`) VALUES
(1, 'Dentist', '', '', 'admin', 1, 'admin'),
(2, 'Staff', '', '', 'staff', 0, 'Client');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dental_procedure`
--
ALTER TABLE `dental_procedure`
  ADD PRIMARY KEY (`procedure_id`);

--
-- Indexes for table `dental_record`
--
ALTER TABLE `dental_record`
  ADD PRIMARY KEY (`record_id`);

--
-- Indexes for table `patient_profile`
--
ALTER TABLE `patient_profile`
  ADD PRIMARY KEY (`patient_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dental_procedure`
--
ALTER TABLE `dental_procedure`
  MODIFY `procedure_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `dental_record`
--
ALTER TABLE `dental_record`
  MODIFY `record_id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `patient_profile`
--
ALTER TABLE `patient_profile`
  MODIFY `patient_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
