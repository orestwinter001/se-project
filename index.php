<?php 
	require_once("public/layouts/header.php");

	if(isset($_SESSION['is_admin'])){
		
		if($_SESSION['is_admin']===0){
			header("Location:user/clientdashboard.php");
		}
		else{
			header("Location:user/admindashboard.php");
		}
	}
	
 ?>


</head>


	<?php require_once("public/layouts/navbar.php"); ?>

		<section class="container">
			<div class="form-group custom-input-space has-feedback">
				<div class="page-heading">
					<h3 class="post-title"></h3>
				</div>
				<div class="page-body clearfix">
					<div class="row">
						<div class="col-md-offset-0 col-md-12">
							<div class="panel panel-default">
								<div class="panel-heading " style="background-color: #337ab7 !important" >
										<center>
										<div class="btn btn-primary" style="background-color: #337ab7 !important;border-color: #337ab7 !important">Admin Dashboard </div>
										</center>

								 	</div>
								
								
								<div class="panel-body">

					  <table class="table table-hover table-responsive" id="dashy">
					    
					    
					  </table>
					</div>
										
									
								


						
					</div>


				</div>
				</div>
			</div>
		</section>






</body>
</html>