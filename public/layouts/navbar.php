
<body>
<section class="container">
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">Dental Clinic</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <ul class="navbar-nav ">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Features</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Pricing</a>
      </li>
      
    </ul>


    
    <ul class="nav navbar-nav navbar-right navbar-expand-lg">

      <!--If the user is Logged Out  -->

      <?php if(!isset($_SESSION['name'])){
        echo '<form class="form-inline my-0 my-lg-3 " method="post" action="model/login.php">
      <input class="form-control  form-control-sm " type="text" placeholder="Username"  id="username" name="username">
      <input class="form-control form-control-sm " type="text" placeholder="Password"  id="password" name="password">
      <button class="btn btn-primary btn-sm mx-2" type="submit">Sign In</button>
    </form> ';
      } 

else{

  echo '<li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
          echo $_SESSION["name"];
        echo '</a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="#">Action</a>
          <a class="dropdown-item" href="#">Another action</a>
          <a class="dropdown-item" href="../model/logout.php">Logout</a>
        </div>
      </li>';
}
      ?>
  



<!--If the user is Logged In  -->

    
  </ul>
  </div>
</nav>
</section>  