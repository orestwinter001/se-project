<?php
require_once("../public/layouts/header.php");

if(isset($_SESSION['is_admin'])){
		
		if($_SESSION['is_admin']!==1){
			header("Location:../user/clientdashboard.php");
		}
		
	}


?>


	<?php require_once("../public/layouts/navbar.php"); ?>


	<section class="container">
			<div class="form-group custom-input-space has-feedback">
				<div class="page-heading">
					<h3 class="post-title"></h3>
				</div>
				<div class="page-body clearfix">
					<div class="row">
						<div class="col-md-offset-0 col-md-12">
							<div class="panel panel-default">
								<div class="panel-heading " style="background-color: #337ab7 !important" >
										<center>
										<div class="btn btn-primary" style="background-color: #337ab7 !important;border-color: #337ab7 !important">Admin Dashboard </div>
										</center>

								 	</div>
								
								
								<div class="panel-body">

					  <table class="table table-hover table-responsive" id="dashy">
					    <tr>
					    	<td>
					    		<div class="col-md-offset-0 col-md-12">
					    			
					    				<div class="active-cyan-4 mb-4">
					    				 <form class="form-inline" method="post" action="../model/getuser.php">	
 										 <input class="form-control" type="text" placeholder="Search Patient"  id="q" name="q" aria-label="Search" >
 										 <button type="submit" class="btn btn-primary">Search</button>

 										 

 										 </form>
 										 
										</div>
					    			
										 Add Patient<br/>	
 										 Accounting Reports
					    		</div>

					    		
					    	</td>
					    	<td>
					    		<div class="col-md-offset-10 col-md-12" >
					    			

										<?php  //foreach( $codes as $index => $code )
												if(isset($_SESSION['patient_name']) ){
													$patient_id = $_SESSION['patient_id'];
													$patient_name = $_SESSION['patient_name'];

													foreach (array_combine($patient_id,$patient_name) as $id=>$name) {
														echo "<a href='clientdashboard.php?id=$id'>".$name."</a><br/>";
													}
												}
												else{
													echo "No Data";
												}

										 ?>
					    		</div>
					    	</td>

					    </tr>
					    
					  </table>
					</div>
										
									
								


						
					</div>


				</div>
				</div>
			</div>
		</section>
</body>



