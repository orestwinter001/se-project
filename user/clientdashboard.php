<?php
require_once("../public/layouts/header.php");
require_once '../config/connect.php';

if(isset($_SESSION['is_admin'])){
		
		if($_SESSION['is_admin']!==0){
			header("Location:../user/admindashboard.php");
		}
		
	}



?>


	<?php require_once("../public/layouts/navbar.php"); ?>


	<section class="container">
			<div class="form-group custom-input-space has-feedback">
				<div class="page-heading">
					<h3 class="post-title"></h3>
				</div>
				<div class="page-body clearfix">
					<div class="row">
						<div class="col-md-offset-0 col-md-12">
							<div class="panel panel-default">
								<div class="panel-heading " style="background-color: #337ab7 !important" >
										<center>
										<div class="btn btn-primary" style="background-color: #337ab7 !important;border-color: #337ab7 !important">Juan Dela Cruz Dashboard </div>
										</center>

								 	</div>
								
								
								<div class="panel-body">


					<?php
					
						$id = $_GET['id'];




					$stmt = $con->prepare("SELECT * FROM patient_profile WHERE patient_id = ?");
					$stmt->bind_param("i", $id);
					$stmt->execute();
					$stmt->store_result();
					if($stmt->num_rows === 0) {
						echo "No Data Found";
						//header("Location:../user/admindashboard.php");
					}

					$stmt->bind_result(
						$patient_id,
						$patient_name,
						$patient_age,
						$patient_contact,
						$patient_gender,
						$patient_address,
						$patient_occupation,
						$patient_status,
						$date,
						$time); 
					$stmt->fetch();
					$stmt->close();


					



					?>				



				
					  <table class="table table-hover table-responsive" id="dashy">
					    	
					    	<thead>
					    		<tr>
					    		 <th scope="col">NAME</th>
					    		 <th scope="col">AGE</th>
					    		 <th scope="col">CONTACT</th>
					    		 <th scope="col">GENDER</th>
							      <th scope="col">ADDRESS</th>
							      <th scope="col">OCCUPATION</th>
							      <th scope="col">STATUS</th>
					    		</tr>

					    	</thead>
					    	<tbody>
					    		<tr>
					    			<?php
					    			echo"
					    			<td>$patient_name</td>
					    			<td>$patient_age</td>
					    			<td>$patient_contact</td>
					    			<td>$patient_gender</td>
					    			<td>$patient_address</td>
					    			<td>$patient_occupation</td>
					    			<td>$patient_status</td>

					    			"

					    			;
					    			?>
					    		</tr>
					    	</tbody>
					    
					  </table>
					<br/><br/>

				  <table class="table table-hover table-responsive" id="dashy">
					    	
					    	<thead>
					    		<tr>
					    		 <th scope="col">DATE</th>
							      <th scope="col">NO.</th>
							      <th scope="col">PROCEDURE</th>
							      <th scope="col">COMPLAIN</th>
							      <th scope="col">TIME</th>
							      <th scope="col">AMOUNT</th>
							      <th scope="col">ENCODED BY</th>
					    		</tr>

					    	</thead>
					    	<tbody>
					    		<tr>
					    			<td>9/11</td>
					    			<td>001</td>
					    			<td>Extraction</td>
					    			<td>Tootache</td>
					    			<td>10:36</td>
					    			<td>1300</td>
					    			<td>Doc Admin</td>

					    		</tr>
					    	</tbody>
					    
					  </table>
					</div>
										
									
								


						
					</div>


				</div>
				</div>
			</div>
		</section>
</body>